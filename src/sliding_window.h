#ifndef SLIDING_WINDOW
#define SLIDING_WINDOW

#include <Arduino.h>

class SlidingWindow {
private:
  uint16_t  cpt;
  uint16_t  size;
  uint16_t* tab;
  uint32_t  acc;
  float     avg;   

public:
  bool debug;
  
  SlidingWindow(uint16_t size);
  void add(uint16_t val) ;
  float getAvg() ;
  
};

#endif 