#ifndef GPIO
#define GPIO

#define THRESHOLD       3550 
#define FREQ_10         10
#define FREQ            2
#define GAS_PER_HOUR    2780.0  // 2780 liter per hour

#define GPIO_PLUS       12
#define GPIO_AUTO       14
#define GPIO_MINUS      27
#define GPIO_HOT_WATER  26
#define GPIO_BOILER     33 
#define GPIO_V4V_POWER  25
#define GPIO_NTC_POWER  32
#define GPIO_V4V        13

#define V4V_SENSOR 

#endif