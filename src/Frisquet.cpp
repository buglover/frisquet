#include "Frisquet.h"
#include "gpio.h"



void Frisquet::boilerStatus(uint16_t adc, bool output) {
  
static uint16_t adcPrev = 0;    // previous adc value
static uint16_t up = 0, down = 0;
  
static uint32_t acc = 0;
static uint16_t cpt = 0;
static uint16_t t1 = 0;
static uint16_t t2 = 0;
static uint16_t peak = 0;
  
static uint32_t upAcc = 0, downAcc = 0, upInc = 0, downInc = 0;

  uint16_t res;

    if (output) {

      //printf("Timer %d, %d, %d, %d \n",boilerValue.down, boilerValue.up, boilerValue.freq, boilerValue.peak);  

      if (upInc) upAvg->add(upAcc/upInc);
      else       upAvg->add(0);
      
      if (downInc) downAvg->add(downAcc/downInc);
      else         downAvg->add(0);
      
      freqAvg->add(upInc + downInc);
      peakAvg->add(peak);

      upAcc   = 0;
      upInc   = 0;
      downAcc = 0;
      downInc = 0;
      peak    = 0;

      t1 = 0;
      t2 = 0;
      acc = 0;
      cpt = 0;
      up = 0;
      down = 0;
      return;
    }

    // if adc is peaking       
    if ((adc > (up + (up-down)/10)) && ((up) && (down))) peak++;
    

    // look for stable value
    if ((abs(adcPrev-adc)<100) || (cpt == 0)) {
       acc += adc;
       cpt++;

       adcPrev = adc;
       return;
    }

    // look for normal square edge (up or down)
    if (abs(adcPrev-adc)<500) 
    {
       res = acc/cpt;

       // define first value
       if (t1==0)  t1 = res;           
       else
         // define second value and swap them if required                      
         if ((t2 == 0) && (abs(t1 - res)>100)) {    
            t2 = res;
            
            if (t1 > t2) {  
              down = t2;
              up = t1;                    
            } else {
              down = t1;
              up = t2;
            }
            //printf("BOILER {%d,%d} - %d\n ",down, up, up-down);            
            
         } 
         else // both Tx set
         {            
            if ((abs(res-down) < 100) && (res > 500)) {
              downAcc += res; 
              downInc ++;            
            }
  
            if ((abs(res-up) < 100) && (res > 500)) {
              upAcc += res;  
              upInc ++;           
            }            
         }                              
        
    } else {
      adcPrev = adc;
      return;  // adc value is exceeding expected range, value is skiped 
    }

    // edge proceeded, reset to track a new one.
    acc = 0; cpt = 0;
    adcPrev = adc;
    return;
}

Frisquet::Frisquet() {
  i2s       = new I2S(I2S_NUM_0, I2S_SAMPLE_RATE, I2S_SAMPLE );
  peakAvg   = new SlidingWindow(5);
  freqAvg   = new SlidingWindow(5);
  upAvg     = new SlidingWindow(5);
  downAvg   = new SlidingWindow(5);
  tempAvg   = new SlidingWindow(10*6);
  
  mcp       = new MCP();
  shift     = 0;
  heatMode  = 0;
  temperature = 0;
  autoMode  = false;
}

float Frisquet::getTemp() {
  return tempAvg->getAvg();
}

void Frisquet::boilerSensorTimer() {
  uint16_t    result, max ;
  int         avg;
  uint16_t*   byteBuffer;
  
  // reader boiler LED, maximum filtering, 500ms 
  i2s->read((adc1_channel_t)ADC1_CHANNEL_5, &byteBuffer, result);

//  printf("BOILER\n");
  for (uint16_t i=0;i<I2S_SAMPLE;i+=4) {
    avg = byteBuffer[i];
    max = byteBuffer[i];
    
    for (byte j=1; j<4; j++) {
      if (max < byteBuffer[i+j]) max = byteBuffer[i+j];
      avg += byteBuffer[i+j];
    }
    avg /= 8;
    
    printf("%d\n", max);
    boilerStatus(max,false);
  }

  
  boilerStatus(0, true);
}

void Frisquet::getBoilerSensor(bool &state, uint16_t &up, uint16_t &down,uint16_t &freq,uint16_t &peak) {

  
  peak  = (uint16_t) peakAvg->getAvg();
  freq  = (uint16_t) freqAvg->getAvg();
  up    = (uint16_t) upAvg->getAvg();
  down  = (uint16_t) downAvg->getAvg();
  
  
  if ((peak>1) && (freq>5))   state=true;
  else                        state=false;

}

void Frisquet::readNTCsensor(float &ohm, float &temp, float &ind, float &tempShift, float &indShift){

// constants
const float adc2ohm3 = -4.8213E-10;
const float adc2ohm2 =  6.0983E-06;
const float adc2ohm1 = -2.7273E-02;
const float adc2ohm0 =  4.2904E+01;

const float adc2ind3 =  1.4186E-08;
const float adc2ind2 = -1.7947E-04;
const float adc2ind1 =  8.0259E-01;
const float adc2ind0 = -1.0041E+03;

const float ind2ohm3 =   1.6459E-10;
const float ind2ohm2 =  -2.3642E-07;
const float ind2ohm1 =  -3.3962E-02;
const float ind2ohm0 =   8.7787E+00;

const float steinhart2 =  8.9573E-04;
const float steinhart1 =  2.7414E-04;
const float steinhart0 = -3.8459E-08;


float     ohmShift;
uint16_t  *byteBuffer, x;

  
  digitalWrite(GPIO_NTC_POWER, HIGH);  
  delay (50);
 
  i2s->read((adc1_channel_t)ADC1_CHANNEL_3, &byteBuffer, x);
 
  digitalWrite(GPIO_NTC_POWER, LOW); 
  //Blynk.virtualWrite(V24, x ); 

    
  ohm  = (pow(x,3)*adc2ohm3) + (pow(x,2)*adc2ohm2) + (x*adc2ohm1) + adc2ohm0;
  temp = 1.0/(steinhart2 + (steinhart1*log(ohm*1000.0)) + (steinhart0 * pow(log(ohm*1000),3))) - 273.15;
  ind  = (pow(x,3)*adc2ind3) + (pow(x,2)*adc2ind2) + (x*adc2ind1) + adc2ind0;

  if (ind < 0)    ind = 0;
  if (ind > 248)  ind = 248;

  tempShift = temp;
  indShift  = ind;
  
  while (shift != 0) {
    
    if (shift > 0) {
      if (tempShift >= temp+shift) break;         
      else indShift++;
    } else {
      if (tempShift <= temp+shift) break;         
      else indShift--;
    }

    if (indShift < 0) {
      indShift = 0;      
      break;
    }
    
    if (indShift > 248) {
      indShift = 248;      
      break;      
    }
     
    ohmShift = (pow(indShift,3)*ind2ohm3) + (pow(indShift,2)*ind2ohm2) + (indShift*ind2ohm1) + ind2ohm0;
    tempShift = 1.0/(steinhart2 + (steinhart1*log(ohmShift*1000.0)) + (steinhart0 * pow(log(ohmShift*1000),3))) - 273.15;
  }

  mcp->write(indShift);

  tempAvg->add(temp);
 
}
void Frisquet::calibration() {
  uint16_t adc_value, *byteBuffer;
  digitalWrite(GPIO_NTC_POWER, HIGH);  
  delay (50);

  Serial.printf("Level, ADC\n");
    
  int level = 0;
  int inc   = 8;
  
  while (1) {
      mcp->write(level);

      i2s->read((adc1_channel_t)ADC1_CHANNEL_3, &byteBuffer, adc_value); 
      Serial.printf("%d, %d\n",level,adc_value);

      level += inc;
      if (level > 256) {
        level = 248;
         inc = -inc;
      }
      
      if (level <0) {
        level = 0;
        inc = -inc;
      }

      delay (250);
  }
}