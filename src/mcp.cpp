#include "mcp.h"
#include <SPI.h>  


MCP::MCP () {     
	// SPI (esp32) - MOSI 23, MISO 19, SCK 18, SS 5
	
  //printf("MCP (MOSI =%d, MISO = %d, SCK = %d, SS = %d)\n",MOSI, MISO, SCK, SS);
	
	SPI.begin(SCK, MISO, MOSI, SS); 
  debug = false;
}

void MCP::write(int16_t value) 
{  
	if (debug) printf("MCP Write (%d)\n",value);

	if (value < 0)    value = 0;  
	if (value > 255)  value = 255;

	digitalWrite(SS,LOW);
	SPI.transfer(B00010001);        // This tells the chip to set the pot
	SPI.transfer((byte)value);      // This tells it the pot position
	digitalWrite(SS,HIGH); 

	delay(10);
}
  
