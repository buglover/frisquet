#ifndef I2S_READER
#define I2S_READER

#include <Arduino.h>
#include <driver/i2s.h>

class I2S {
private:
  uint16_t*       byteBuffer[7];
  i2s_port_t      port;
  i2s_config_t    i2s_config ;
  byte            zeroCpt;
  byte            lastChannel;
   
public:
  bool    debug;
  
  I2S(i2s_port_t port, int  freq, int bufferSize); 
  void reset(); 
  uint16_t read(adc1_channel_t channel, uint16_t** output, uint16_t &average);

};

#endif 