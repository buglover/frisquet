#include "sliding_window.h"

SlidingWindow::SlidingWindow(uint16_t size) {
    this->size = size;

    tab = (uint16_t*) malloc(sizeof(uint16_t) * size);
    for (int i=0;i<size;i++) tab[i] = 0;
  
    acc = 0;
    avg = 0;
    cpt = 0;

    debug=false;
}

void SlidingWindow::add(uint16_t val) {
    if (debug) {
      Serial.printf("Acc=%d , Cpt=%d  +%d  -%d [",acc, cpt,(int)val , (int)tab[cpt] );
      for (int i=0;i<32; i++)  Serial.printf("%d ",tab[i]);
      Serial.printf("]\n");
    }
    
    acc += val;
    acc -= tab[cpt];
    tab[cpt] = val;
    cpt = (cpt+1) % size;

}

float SlidingWindow::getAvg() {
    return (float)acc / size;
}
  
