#include "domoticz.h"

Domoticz::Domoticz(char* address, uint16_t port) {
  const size_t capacity = 2*JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(16) + JSON_OBJECT_SIZE(34) + 810;
  
  sprintf(base,"http://%s:%d",address, port);
  doc = new DynamicJsonDocument(capacity);  
  lastRid = 0;
  debug = false;
}

bool Domoticz::getField(uint16_t rid, char* field, float &val) {
  if (lastRid != rid) {
    sprintf(request,"%s/json.htm?type=devices&rid=%d",base,rid);
    http.begin(request);  
    
    int error= http.GET() ;
    if (debug) printf("Domoticz.getField (%d) : %s\n",error, request);
    
    if (error == 200) { //Check the returning code
      deserializeJson(*doc, http.getString());        
      val = (*doc)["result"][0][field];     
      if (debug) printf("Domoticz : %s = %f\n",field, val);
      lastRid = rid;        
      return true;  
    } else {
      printf("URL %s\nHTTP ERROR :%d / %s\n",request, error, http.errorToString(error).c_str());
      return false;   
    }
  } else {
    val = (*doc)["result"][0][field];     
    return true;  
  }
}

bool Domoticz::setValue(uint16_t rid, float val) {
  sprintf(request,"%s/json.htm?type=command&param=udevice&idx=%d&nvalue=0&svalue=%.6f",base, rid, val);
  http.begin(request);  

  
  int error= http.GET() ;
  if (debug) printf("Domoticz.setValue : (%d) %s\n",error, request);
  
  if (error == 200) return true;
  else {
    printf("URL %s\nHTTP ERROR :%d / %s\n",request, error, http.errorToString(error).c_str());
    return false;
  } 
}
