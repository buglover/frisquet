#include <Time.h>
#include <TimeLib.h>

/*MCP41010 Tutorial*/

#include <math.h>    // (no semicolon)
#include <driver/adc.h>


#include "sliding_window.h"
#include "domoticz.h"
#include "Frisquet.h"


#include <esp_wifi.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <HTTPClient.h>


// for syslog
#include <WiFiUdp.h>      
#include <Syslog.h>
#define SYSLOG_SERVER "192.168.0.8"
#define SYSLOG_PORT 514
#define DEVICE_HOSTNAME "ESP32"
#define APP_NAME "Frisquet"     

// A UDP instance to let us send and receive packets over UDP
WiFiUDP udpClient;

// Create a new syslog instance with LOG_KERN facility
Syslog syslog(udpClient, SYSLOG_SERVER, SYSLOG_PORT, DEVICE_HOSTNAME, APP_NAME, LOG_KERN);




// for UpdateOTA
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

// for Blynk
//#define BLYNK_DEBUG // Optional, this enables lots of prints
#include <BlynkSimpleEsp32.h>
#include <TimeLib.h>
#include <WidgetRTC.h>

/* blynk MAP
 *  
 * V0 - LED boiler
 * V1 - Boiler IT rate
 * V3 - LED ECS
 * V4 - V4V
 * V5 - Boiler (Up)
 * V6 - Boiler (Freq)
 * V7 - Boiler (Peak)
 * V8 - Boiler (Down)
 * V9 - Boiler (Temp Avg)
 * 
 * V10 - Inside   Temperature 
 * V11 - Outside  Temperature 
 * V12 - Delta    Temperature
 * V13 - Terrase  Temperature
 * V14 - Window   Temperature
 * 
 * V15 - Command  Minus
 * V16 - Command  Plus
 * 
 * V20 - Auto(1) / Manuel (2)         (2 state button)
 * V21 - Eco(1) / Stop (2) / Max (3)  (3 states Button)
 * V22 - Temperature control          (Slider 15 - 85)
 * V23 - Shift                        (-20  <->  +20) 
 * V24 - NTC reading from NTC         (0 - 4096)  / (0 Ohm to 10 Ohm)
 * V30 - Ohm (calculated)
 * V31 - Temp (calculated)
 * V32 - Index (calculated)
 * V41 - Temp (calculated)
 * V42 - Index (calculated)
 * 
 * V50 - Terminal
 * V51 - Reset button
 */

// to calibation ADC reading with MCP40010
//#define CALIBRATION
//#define IS2_READING


#define BLYNK_PRINT Serial
#define BLYNK_MAX_READBYTES 2048
#define debug 1

#define BLYNK_GREEN     "#23C48E"
#define BLYNK_BLUE      "#04C0F8"
#define BLYNK_YELLOW    "#ED9D00"
#define BLYNK_RED       "#D3435C"
#define BLYNK_DARK_BLUE "#5F7CD8"
#define BLYNK_GRAY      "#212227"



// Boiler control
#include "gpio.h"

 

// Blink 
char  auth[]   = "6U1y3m5jtgTLconHY5l-ZjUkA6BkmPVl";    // Main
//char  auth[] = "QlfJvprM-fWa7-xlELtqQ5MX_SIDGjW_";
char  ssid[]   = "DigitalFountain2G";
char  pass[]   = "SophieNicolasJulietteMargaux@2006";
char  server[] = "192.168.0.8";
int   port     = 8082;

BlynkTimer      blynkTimer;
WidgetLED       ledBoiler(V0);
WidgetLED       ledECS(V3);
WidgetLED       ledV4V(V4);
WidgetTerminal  terminal(V50);
WidgetRTC       rtc;


#define FREQ_10   10
bool  resetRequested = false; 



Domoticz      domoticz("192.168.0.8",8080);
Frisquet      frisquet;




void pressButton(int pin) {  
  digitalWrite(pin, HIGH); 
  delay(400);
  digitalWrite(pin, LOW); 
  delay(400);
}







// Digital clock display of the time
void clockDisplay()
{
  // You can call hour(), minute(), ... at any time
  // Please see Time library examples for details

  String currentTime = "Boot - " + String(hour()) + ":" + minute() + ":" + second() +" - "+String(day()) + "/" + month() + "/" + year();;
  Serial.print("Current time: ");
  Serial.println(currentTime);
  syslog.logf(LOG_INFO,"%s",currentTime.c_str());

  terminal.println(currentTime);
  terminal.flush();
}


void setBoilerTimer()
{
uint32_t temp;

}


void myBoilerSensorTimer() {
  frisquet.boilerSensorTimer();
}

void myBlynkTimerEvent()
{
  static float interieur, exterieur1, exterieur2, exterieur;
  float ohm, temp, ind;
  float tempShift, indShift;  
  float consomation = GAS_PER_HOUR / (3600.0 * 10) * FREQ_10;
  bool  boiler = false;
  bool  ecs = false;
 
  uint16_t *byteBuffer;
  uint16_t  up, down, freq, peak;

  syslog.logf(LOG_INFO,"blynkTimerEvent");

  if(not Blynk.connected() ) {
    Serial.println("blynk disconnected ...");
    syslog.logf(LOG_ERR,"Blynk disconnected ...");
 
    delay (100);
    resetRequested = true;
    
    return;
  }
  
  // get Boiler state
  frisquet.getBoilerSensor(boiler, up, down, freq, peak);
  Blynk.virtualWrite(V5, up   ); 
  Blynk.virtualWrite(V8, down ); 
  Blynk.virtualWrite(V6, freq ); 
  Blynk.virtualWrite(V7, peak ); 

  //Serial.printf("BoilerSensor %d (%d) \n",val, boiler);

  // get V4V state 
  digitalWrite(GPIO_V4V_POWER, HIGH);  
  delay (50);
  ecs = digitalRead(GPIO_V4V) ? false : true ;
  delay(50);
  digitalWrite(GPIO_V4V_POWER, LOW);  


  if (boiler) {
    
    // Notify Boiler consumption
    if (!domoticz.setValue(2472, consomation)) {
      Serial.println("ERROR - boiler");
      syslog.logf(LOG_ERR,"Boiler error");
    }   
    
    if (ecs) {
      ledECS.on();
      ledBoiler.off();
    }
    else {
      ledBoiler.on();    
      ledECS.off();
    }
  }
  else {
    ledBoiler.off();
    ledECS.off();
  }

  
  frisquet.readNTCsensor(ohm, temp, ind, tempShift, indShift);
  
  Blynk.virtualWrite(V30, int(ohm*1000.0) );
  Blynk.virtualWrite(V31, int(temp*10)/10.0 );
  Blynk.virtualWrite(V32, int(ind) );
  Blynk.virtualWrite(V42, int(indShift) );
  Blynk.virtualWrite(V41, int(tempShift*10)/10.0 );

  Blynk.virtualWrite(V9, int(frisquet.getTemp()*10)/10.0 );
  //Serial.printf("BoilerAvgTemp %f \n",frisquet.getTemp());


  if ((ind > 0) || (boiler)) {
    ledV4V.on();
    if (ecs)  Blynk.setProperty(V4, "color",BLYNK_DARK_BLUE);
    else      Blynk.setProperty(V4, "color",BLYNK_RED);    
  } 
  else ledV4V.off();
  

   // Notify Temp sortie
  if (!domoticz.setValue(2580, temp)) {
    Serial.println("ERROR - Temp sortie");
    syslog.logf(LOG_ERR,"Temp sortie");
  }   
 

  // 1948 : Xiron5
  if (domoticz.getField(1948,"Temp", interieur)) {
    Blynk.virtualWrite(V10,interieur);
  }

  // 1944 : Xiron1 * 75%
  // 3302 : Temperature capteur pluie * 25%
  if (domoticz.getField(3302, "Temp", exterieur1) && domoticz.getField(1944, "Temp", exterieur2)) {
    exterieur = (exterieur1+exterieur2*3)/4;
    Blynk.virtualWrite(V13, exterieur1);
    Blynk.virtualWrite(V14, exterieur2);
    Blynk.virtualWrite(V11, exterieur);
  }

  Blynk.virtualWrite(V12, interieur - exterieur);
   
}
 

BLYNK_CONNECTED() {
  Serial.println("BLYNK connected");
  syslog.logf(LOG_INFO,"BLYNK connected");
  // Synchronize time on connection
  rtc.begin();
}

BLYNK_WRITE(V15) // Minus button
{
  Serial.printf("BLYNK_WRITE V15 - Minus Button : %d\n",param.asInt());
  syslog.logf(LOG_INFO,"BLYNK_WRITE V15 - Minus Button : %d",param.asInt());
  if (param.asInt() == 1) {
    pressButton(GPIO_MINUS);
    frisquet.temperature--;
    Blynk.virtualWrite(V22, frisquet.temperature*5+15);
  }
}

BLYNK_WRITE(V16) // Plus button
{
  Serial.printf("BLYNK_WRITE V16 - Plus Button : %d\n",param.asInt());
  syslog.logf(LOG_INFO,"BLYNK_WRITE V16 - Plus Button : %d",param.asInt());
  if (param.asInt() == 1) {
    pressButton(GPIO_PLUS); 
    frisquet.temperature++;
    Blynk.virtualWrite(V22, frisquet.temperature*5+15);
  }
}



BLYNK_WRITE(V20) // Auto/Manual
{
  frisquet.autoMode = !frisquet.autoMode;
  Serial.println("BLYNK_WRITE V20 - Auto / Manual");
  syslog.logf(LOG_INFO,"BLYNK_WRITE V20 - Auto / Manual - %d",frisquet.autoMode);
  
  pressButton(GPIO_AUTO);

  Serial.printf("autoMode : %d\n",frisquet.autoMode);


  if (frisquet.autoMode == true)  Blynk.setProperty(V22, "color",BLYNK_GRAY);
  else                   Blynk.setProperty(V22, "color",BLYNK_YELLOW);
  
}


BLYNK_WRITE(V21) // Eco/Stop/Max
{
  Serial.printf("BLYNK_WRITE V21 - Eco/Stop/Max - %d\n",param.asInt()-1);
  syslog.logf(LOG_INFO,"BLYNK_WRITE V21 - Eco/Stop/Max - %d",param.asInt()-1);
 
  int pinValue = param.asInt()-1; // assigning incoming value from pin V1 to a variable

  switch (pinValue) // target value
  { 
    case 0: // Eco
      Blynk.setProperty(V21, "color",BLYNK_GREEN);
 
      if (frisquet.heatMode == 1) pressButton(GPIO_HOT_WATER);     // Stop --> Eco
      if (frisquet.heatMode == 2) {                          // Max  --> Eco
        pressButton(GPIO_HOT_WATER); 
        pressButton(GPIO_HOT_WATER); 
      }  
      
      break;
    case 1: // Stop
     Blynk.setProperty(V21, "color",BLYNK_RED);
 
      if (frisquet.heatMode == 2) pressButton(GPIO_HOT_WATER);     // Max  --> Stop
      if (frisquet.heatMode == 0) {                          // Eco  --> Stop
        pressButton(GPIO_HOT_WATER); 
        pressButton(GPIO_HOT_WATER); 
      }  
   

      break;
      
    case 2: // Max
      Blynk.setProperty(V21, "color",BLYNK_YELLOW);
 
      if (frisquet.heatMode == 0) pressButton(GPIO_HOT_WATER);     // Eco --> Max
      if (frisquet.heatMode == 1) {                          // Stop --> Max
        pressButton(GPIO_HOT_WATER); 
        pressButton(GPIO_HOT_WATER); 
      }  
     
      break;
  }

  frisquet.heatMode = pinValue;
  
}


BLYNK_WRITE(V22) // Control Temp
{
  Serial.printf("BLYNK_WRITE V22 - Control Temperature - %d\n ",param.asInt());
  syslog.logf(LOG_INFO,"BLYNK_WRITE V22 - Control Temperature - %d",param.asInt());
  
  int pinValue = param.asInt(); // assigning incoming value from pin V1 to a variable
  Serial.println(pinValue);
  
  pinValue = (pinValue-15)/5;
  
  if (pinValue == 0)  Blynk.setProperty(V22, "color",BLYNK_RED);
  else                Blynk.setProperty(V22, "color",BLYNK_YELLOW);
      
  Serial.print("Old : ");
  Serial.print(frisquet.temperature);
  Serial.print("  New : ");
  Serial.print(pinValue);
  Serial.print("  Up(1)/Down(0) : ");
  Serial.println ( (pinValue - frisquet.temperature)>0 );

  if (frisquet.temperature != pinValue) {  
    for (int i = 0; i<abs(pinValue - frisquet.temperature);i++) {
        if ((pinValue - frisquet.temperature)>0) 
          pressButton(GPIO_PLUS);
        else
          pressButton(GPIO_MINUS);
      }
      
      frisquet.temperature = pinValue;
  }
}


BLYNK_WRITE(V23) // NTC control (write)
{
  Serial.printf("BLYNK_WRITE V23 - slider : %d\n",param.asInt());
  syslog.logf(LOG_INFO,"BLYNK_WRITE V23 - slider : %d",param.asInt());
  
  frisquet.shift = param.asInt(); // assigning incoming value from pin V1 to a variable  
}


BLYNK_WRITE(V51) // Restart button
{
  Serial.printf("BLYNK_WRITE V51 - Restart Button : %d\n",param.asInt());
  syslog.logf(LOG_INFO,"BLYNK_WRITE V51 - Restart Button : %d",param.asInt());
  if (param.asInt() == 1) {
   
    syslog.logf(LOG_INFO,"Disable WIFI");
    
    delay(500);
    WiFi.mode(WIFI_OFF);
  }
}



void OTA() {
  static int cpt = 0;

  
 // Hostname defaults to esp3232-[MAC]
  ArduinoOTA.setHostname("Frisquet");
  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
      syslog.logf(LOG_INFO,"OTA 1/3 - Start updating %s ",type.c_str());
      cpt = 0;
    })
    .onEnd([]() {
      Serial.println("\nEnd");
      syslog.logf(LOG_INFO,"OTA 3/3 - End");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      int pourcentage = int(progress / (total / 100));

      if (pourcentage >= cpt) {
        Serial.printf("Progress: %u%%\r", pourcentage);
        syslog.logf(LOG_INFO,"OTA 2/3 - Progress: %u%%", pourcentage);
        cpt+= 10;
      }
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      syslog.logf(LOG_ERR,"OTA x/3 - Error[%u]: ", error);
      
      if (error == OTA_AUTH_ERROR)          syslog.logf(LOG_ERR,"Auth Failed");
      else if (error == OTA_BEGIN_ERROR)    syslog.logf(LOG_ERR,"Begin Failed");
      else if (error == OTA_CONNECT_ERROR)  syslog.logf(LOG_ERR,"Connect Failed");
      else if (error == OTA_RECEIVE_ERROR)  syslog.logf(LOG_ERR,"Receive Failed");
      else if (error == OTA_END_ERROR)      syslog.logf(LOG_ERR,"End Failed");
    });

  ArduinoOTA.begin();  
}




void setup() {  
int retryCounter = 50;
  
  Serial.begin(115200); 
  while(!Serial) {} // Wait 

  pinMode(GPIO_PLUS,      OUTPUT);
  pinMode(GPIO_AUTO,      OUTPUT);
  pinMode(GPIO_MINUS,     OUTPUT);
  pinMode(GPIO_HOT_WATER, OUTPUT);
  pinMode(GPIO_NTC_POWER, OUTPUT);
  pinMode(ADC1_CHANNEL_3, INPUT);
  pinMode(ADC1_CHANNEL_5, INPUT);

  pinMode(SS,             OUTPUT);     
  
  digitalWrite(GPIO_PLUS,       LOW);  
  digitalWrite(GPIO_AUTO,       LOW);  
  digitalWrite(GPIO_MINUS,      LOW);  
  digitalWrite(GPIO_HOT_WATER,  LOW);  
  digitalWrite(GPIO_NTC_POWER,  LOW);  
  
  pinMode(GPIO_V4V_POWER      , OUTPUT);
  pinMode(GPIO_V4V            , INPUT_PULLUP);
  digitalWrite(GPIO_V4V_POWER , LOW); 



  adc1_config_width(ADC_WIDTH_12Bit);
  adc1_config_channel_atten(ADC1_CHANNEL_3,ADC_ATTEN_11db);   // NTC
  adc1_config_channel_atten(ADC1_CHANNEL_5,ADC_ATTEN_11db);   // Boiler
  
 
  WiFi.begin(ssid, pass);  
  while ((WiFi.status() != WL_CONNECTED) && (retryCounter>0)){
    delay(500);
    retryCounter--;
    Serial.print(".");
  }
  
  if (retryCounter == 0) 
  {
    esp_sleep_enable_timer_wakeup(100);
    esp_deep_sleep_start();        
  }
    
  esp_wifi_set_ps(WIFI_PS_NONE);        // To disable power saving
  // https://github.com/espressif/esp-idf/blob/master/examples/wifi/power_save/main/power_save.c

  delay(300);
  //IPAddress ip = WiFi.localIP();
  //Serial.printf("WiFi connected %d.%d.%d.%d\n",ip[0],ip[1],ip[2],ip[3]);  
  Serial.printf("Setup 1/3 - address : %s",WiFi.localIP().toString().c_str());
  syslog.logf(LOG_INFO,"Setup 1/3 - address : %s",WiFi.localIP().toString().c_str());


  OTA();

 

  

#ifndef CALIBRATION
  if (retryCounter > 0) {
    Blynk.begin(auth, ssid, pass, server, port); 
    syslog.logf(LOG_INFO,"Setup 1/3 - address : %s",WiFi.localIP().toString().c_str());  // BUG 
        
    Serial.println("Setup 2/3 - Blynk connected");
    syslog.logf(LOG_INFO,"Setup 2/3 - Blynk connected");
 
    // Gray the Speed
    Blynk.virtualWrite(V0, frisquet.heatMode+1);
  
    frisquet.autoMode = true;
    Blynk.virtualWrite(V20,frisquet.autoMode?1:0);
    if (frisquet.autoMode == true)  Blynk.setProperty(V22, "color",BLYNK_GRAY);
    else                   Blynk.setProperty(V22, "color",BLYNK_YELLOW);
  
    
    Blynk.virtualWrite(V21,"color",BLYNK_GREEN);
    Blynk.virtualWrite(V21,1);
  
   // Setup a function to be called every 10s second
    blynkTimer.setInterval(FREQ_10*1000/2,        myBlynkTimerEvent);
    blynkTimer.setInterval(FREQ_10*1000/5,        myBoilerSensorTimer);
    blynkTimer.setInterval(FREQ_10*1000*6*FREQ,    setBoilerTimer);
  
    Blynk.virtualWrite(V23,frisquet.shift);

    Serial.println("Setup 3/3 - Completed");
    syslog.logf(LOG_INFO,"Setup 3/3 - Completed");
    clockDisplay();
   }
#else
  frisquet.calibration();
#endif   
}

void loop()
{
  blynkTimer.run(); 
  Blynk.run();
  if (resetRequested) {
    syslog.logf(LOG_INFO,"ESP will restart");
    delay (100);
    esp_sleep_enable_timer_wakeup(100);
    esp_deep_sleep_start();        
  }
  ArduinoOTA.handle();
}