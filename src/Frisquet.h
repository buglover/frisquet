#ifndef FRISQUET
#define FRISQUET

#include <Arduino.h>
#include "I2S_reader.h"
#include "sliding_window.h"
#include "mcp.h"


class Frisquet {
private:
const     int I2S_SAMPLE_RATE =     8000;
const     int I2S_SAMPLE      =     1000;

  I2S       *i2s;
  MCP       *mcp;

  SlidingWindow *peakAvg;
  SlidingWindow *freqAvg;
  SlidingWindow *upAvg;
  SlidingWindow *downAvg;
  SlidingWindow *tempAvg;
 
  
  void boilerStatus(uint16_t adc, bool output);

public:  
  int16_t		      shift;
  bool            heatMode;
  byte            temperature;
  bool            autoMode;
  
  Frisquet() ;
  void  boilerSensorTimer();
  void  getBoilerSensor(bool &state, uint16_t &up, uint16_t &down,uint16_t &freq,uint16_t &peak);
  void  readNTCsensor(float &ohm, float &temp, float &ind, float &tempShift, float &indShift);
  void  calibration();
  float getTemp();
  
};

#endif
