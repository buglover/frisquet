#ifndef DOMOTICZ
#define DOMOTICZ

#include <Arduino.h>
#include <HTTPClient.h> 
#include <ArduinoJson.h>

class Domoticz {
private:
  DynamicJsonDocument* doc;

  char base[30];
  char request[120];
  HTTPClient http;
  uint16_t lastRid;
  
public:
  bool  debug;
  Domoticz(char* address, uint16_t port);
  bool getField(uint16_t rid, char* field, float &val);
  bool setValue(uint16_t rid, float val);
};

#endif
