#include "I2S_reader.h"
#include <driver/adc.h>
#include "soc/syscon_reg.h"
#include "soc/periph_defs.h"

#define MAX_RETRY 5

I2S::I2S(i2s_port_t port, int  freq, int bufferSize) {  
  this->port = port;
  
  i2s_config.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX | I2S_MODE_ADC_BUILT_IN);
  i2s_config.sample_rate = freq;            
  i2s_config.dma_buf_len = bufferSize;                   
  i2s_config.bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT;
  i2s_config.channel_format = I2S_CHANNEL_FMT_ONLY_RIGHT;   //
  i2s_config.use_apll = false;      // disable high precision clock
  i2s_config.communication_format = I2S_COMM_FORMAT_I2S_MSB;
  i2s_config.intr_alloc_flags = ESP_INTR_FLAG_LEVEL1;
  i2s_config.dma_buf_count = 2;

  ESP_ERROR_CHECK( i2s_driver_install(port, &i2s_config,  0, NULL) );
  ESP_ERROR_CHECK( adc1_config_width(ADC_WIDTH_12Bit) );

  for (int i=0;i<7;i++)
    byteBuffer[i] = NULL;

  SET_PERI_REG_MASK(SYSCON_SARADC_CTRL2_REG, SYSCON_SARADC_SAR1_INV);   // to invert data
  debug = false;
  zeroCpt = 0;
  lastChannel = 255;
     
}

void I2S::reset() {
  if (debug)        printf("- - - - - RESET   I2S - - - - \n");

  ESP_ERROR_CHECK( i2s_driver_uninstall(port) );
  periph_module_reset(PERIPH_I2S0_MODULE);
  delay(10);
  ESP_ERROR_CHECK( i2s_driver_install(port, &i2s_config,  0, NULL) );
  SET_PERI_REG_MASK(SYSCON_SARADC_CTRL2_REG, SYSCON_SARADC_SAR1_INV);   // to invert data
}

// return number of sample read
uint16_t I2S::read(adc1_channel_t channel, uint16_t** output, uint16_t &average) {
  size_t        bytesRead = 0;
  unsigned int  acc = 0;
  byte          chn;
  
  if (byteBuffer[(int)channel] == NULL) {     
    (*output)  = (uint16_t*)malloc(sizeof(uint16_t) * i2s_config.dma_buf_len);
    byteBuffer[(int)channel] = *output;  
  }
  else {
    (*output) = byteBuffer[(int)channel];
  }
  
  ESP_ERROR_CHECK( i2s_set_adc_mode(ADC_UNIT_1, channel) );

  if (lastChannel != channel) {
    ESP_ERROR_CHECK( i2s_adc_enable(port) );

    // wait to get a first buffer full
    delay(i2s_config.dma_buf_len*1000/i2s_config.sample_rate+10);
    
    lastChannel = channel;
  }
  
  ESP_ERROR_CHECK( i2s_read( port, (void*) (*output), sizeof(uint16_t) * i2s_config.dma_buf_len, &bytesRead, portMAX_DELAY) );
  ESP_ERROR_CHECK( i2s_adc_disable(port) );

  chn = (*output)[0]>>12; 
  //printf("CHN = %d / %d ",chn, channel);
  for (int i=0;i<i2s_config.dma_buf_len; i++)  { 
    //if ((*output)[i]>>12 != channel)  printf(">> Buf[%d] = %d\n",i,(*output)[i]>>12);
    
    (*output)[i] &= 0xFFF;  // remove the ADC channel
    acc += ((*output)[i]);
  }
  average = acc / i2s_config.dma_buf_len;

  // Check initialisation correct
  if (average == 0) {
    zeroCpt++;

    if (zeroCpt == MAX_RETRY) {
      printf(" .  RESET .  \n");
      esp_sleep_enable_timer_wakeup(100);
      esp_deep_sleep_start();        
    }
  } else {
    zeroCpt = 0;
  }
  
  
  if (debug) {
    printf("[%d]  / %d - ",channel,  (average/16)) ; 
    for (int i=0;i<32;i++) 
      printf("%d ", ((*output)[i] / 16));
    printf(" ----> %d \n",bytesRead);

  }
  
  return bytesRead/2;
}
