#include <driver/i2s.h>
#include <driver/adc.h>


#define BLYNK_DEBUG // Optional, this enables lots of prints
#include <BlynkSimpleEsp32.h>
#include <esp_wifi.h>
#include <WiFi.h>
#include <WiFiClient.h>


char  auth[]   = "ZtmIHHXjq8TvI9-OKV2hqgUA-SlqsE6u";
char  ssid[]   = "DigitalFountain2G";
char  pass[]   = "SophieNicolasJulietteMargaux@2006";
char  server[] = "192.168.0.8";
int   port     = 8082;


int val = 0;
int inc = +16;
const int ledChannel = 0;

#define SAMPLING_FREQUENCY        16000
#define MAX_SAMPLES               1000



// SampleBuffferInitI2S
// 
// Do the initial setup required if we're going to capture the audio with I2S

//i2s number
#define EXAMPLE_I2S_NUM           (I2S_NUM_0)
//i2s sample rate
//#define EXAMPLE_I2S_SAMPLE_BITS   (I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB)
//enable display buffer for debug
#define EXAMPLE_I2S_BUF_DEBUG     (0)
//I2S read buffer length
#define EXAMPLE_I2S_READ_LEN      (MAX_SAMPLES)
//I2S built-in ADC unit
#define I2S_ADC_UNIT              ADC_UNIT_1
//I2S built-in ADC channel
#define I2S_ADC_CHANNEL           ADC1_CHANNEL_0


class I2S {
private:
  uint16_t* byteBuffer[7];
  uint16_t  bufferSize;
  i2s_port_t  port;
   
public:
   I2S(i2s_port_t port, int  freq, int bufferSize) {
  
    this->port = port;
    this->bufferSize = bufferSize;

    printf("Buffer size  %d\n",bufferSize);
    
    i2s_config_t i2s_config ;
    i2s_config.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX | I2S_MODE_ADC_BUILT_IN);
    i2s_config.sample_rate = freq;            
    i2s_config.dma_buf_len = bufferSize;                   
    i2s_config.bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT;
    i2s_config.channel_format = I2S_CHANNEL_FMT_ONLY_RIGHT;  
    i2s_config.use_apll = false;
    i2s_config.communication_format = I2S_COMM_FORMAT_I2S_MSB;
    i2s_config.intr_alloc_flags = ESP_INTR_FLAG_LEVEL1;
    i2s_config.dma_buf_count = 2;

    ESP_ERROR_CHECK( i2s_driver_install(port, &i2s_config,  0, NULL) );
    ESP_ERROR_CHECK( adc1_config_width(ADC_WIDTH_12Bit) );

    for (int i=0;i<7;i++)
      byteBuffer[i] = NULL;
     
  }

  uint16_t read(adc1_channel_t channel, uint16_t** output) {
    size_t bytesRead;
    
    if (byteBuffer[(int)channel] == NULL) {
      *output  = (uint16_t*)malloc(sizeof(uint16_t)*bufferSize);
      byteBuffer[(int)channel] = *output;  
    }
    else {
      *output = byteBuffer[(int)channel];
    }
    
    ESP_ERROR_CHECK( i2s_set_adc_mode(ADC_UNIT_1, channel) );
    ESP_ERROR_CHECK( i2s_adc_enable(port) );
    ESP_ERROR_CHECK( i2s_read( port, (void*) (*output), sizeof(uint16_t)*bufferSize, &bytesRead, portMAX_DELAY) );
    ESP_ERROR_CHECK( i2s_adc_disable(port) );


    unsigned int acc = 0;
     for (int i=0;i<MAX_SAMPLES;i++) {
       (*output)[i] &= 0xFFF;
       acc += ((*output)[i]);
     }

    printf("[%d]  / %d - ",channel, 255 - (acc/bufferSize/16)) ; 
    for (int i=0;i<32;i++) 
      printf("%d ", 255 - ((*output)[i] / 16));
    printf(" ----> %d \n",bytesRead);

   return bytesRead;
  }

  void dump(adc1_channel_t channel) {

    uint16_t* buffer = byteBuffer[(int)channel];
    unsigned int acc = 0;
   
    for (int i=0; i<bufferSize; i++) 
      acc += ((buffer[i] & 0xFFF) / 16);

    acc = acc/bufferSize;
  
    printf("%d Average  %d :", channel, acc); 
  
    if ((int)channel==0) 
      Blynk.virtualWrite(V11,  acc );       
    else 
      Blynk.virtualWrite(V13,  acc ); 
    
    for (int i=0;i<32;i++) 
      printf("%d ", (buffer[i] & 0xFFF));
      
    printf("\n");
          
  }
};


//flash record size, for recording 5 second
void SampleBufferInitI2S()
{
  i2s_config_t i2s_config;
  i2s_config.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX | I2S_MODE_ADC_BUILT_IN);
  i2s_config.sample_rate = SAMPLING_FREQUENCY;            
  i2s_config.dma_buf_len = MAX_SAMPLES;                   
  i2s_config.bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT;
  i2s_config.channel_format = I2S_CHANNEL_FMT_ONLY_LEFT;  
  i2s_config.use_apll = false;
  i2s_config.communication_format = I2S_COMM_FORMAT_I2S_MSB;
  i2s_config.intr_alloc_flags = ESP_INTR_FLAG_LEVEL1;
  i2s_config.dma_buf_count = 2;

  //install and start i2s driver

  ESP_ERROR_CHECK( i2s_driver_install(EXAMPLE_I2S_NUM, &i2s_config,  0, NULL) );
  

  ESP_ERROR_CHECK( adc1_config_width(ADC_WIDTH_12Bit) );
  ESP_ERROR_CHECK( adc1_config_channel_atten(ADC1_CHANNEL_0,ADC_ATTEN_11db) ); 
  ESP_ERROR_CHECK( adc1_config_channel_atten(ADC1_CHANNEL_3,ADC_ATTEN_11db) ); 

}

I2S i2s(I2S_NUM_0, SAMPLING_FREQUENCY, MAX_SAMPLES);


void FillBufferI2S(int port)
{
  uint16_t byteBuffer[MAX_SAMPLES];

  size_t bytesRead = 0;
  int acc = 0;

  if (port==0) {
    ESP_ERROR_CHECK( adc_gpio_init(ADC_UNIT_1, ADC_CHANNEL_0) );
    ESP_ERROR_CHECK( i2s_set_adc_mode(I2S_ADC_UNIT, ADC1_CHANNEL_0) );   
  }
  else {
    ESP_ERROR_CHECK( adc_gpio_init(ADC_UNIT_1, ADC_CHANNEL_3) );
    ESP_ERROR_CHECK( i2s_set_adc_mode(I2S_ADC_UNIT, ADC1_CHANNEL_3) );   
  }
  
  ESP_ERROR_CHECK( i2s_adc_enable(EXAMPLE_I2S_NUM) );
  ESP_ERROR_CHECK( i2s_read(EXAMPLE_I2S_NUM, (void*) byteBuffer, sizeof(byteBuffer), &bytesRead, portMAX_DELAY) );
  ESP_ERROR_CHECK( i2s_adc_disable(EXAMPLE_I2S_NUM) );


 for (int i=0;i<MAX_SAMPLES;i++) {
   byteBuffer[i] &= 0xFFF;
   acc += (byteBuffer[i]);
 }

  printf("%d Average - %d :",port, (4096-acc/MAX_SAMPLES)/16); 

  if (port==0) {
    Blynk.virtualWrite(V11,  (4096-acc/MAX_SAMPLES)/16 ); 
  }
  else {
    Blynk.virtualWrite(V13,  (4096-acc/MAX_SAMPLES)/16 ); 
  }
  
  for (int i=0;i<32;i++) 
    printf("%d ",  256 - (byteBuffer[i] / 16)   );
  printf(" ----> %d \n",bytesRead);
 
/*   
  printf("adc_data_buf[0] = %d adc_data_buf[1] = %d adc_data_buf[2] = %d\n",byteBuffer[0],byteBuffer[1],byteBuffer[2]);

  if(((byteBuffer[0]) == 0) && ((byteBuffer[1]) == 0) && ((byteBuffer[2]) == 0)) {
    printf("adc_data_buf[0] = %d adc_data_buf[1] = %d adc_data_buf[2] = %d\n",byteBuffer[0],byteBuffer[1],byteBuffer[2]);
          // If the adc reads zero values, it means it hasn't been started correctly. Restart it again to fix this problem.       
  }
  else
    printf("NO ZERO\n");
  
*/
}
  
void setup() {
  // put your setup code here, to run once:
  int retryCounter = 400;
  // setting PWM properties
const int freq = 1000;
const int resolution = 8;


  Serial.begin(115200); 
  while(!Serial) {} // Wait 

  WiFi.begin(ssid, pass);  
  while ((WiFi.status() != WL_CONNECTED) && (retryCounter>0)){
    delay(500);
    retryCounter--;
    Serial.print(".");
  }
  
  if (retryCounter == 0) ESP.restart();

  esp_wifi_set_ps(WIFI_PS_NONE);        // To disable power saving
  // https://github.com/espressif/esp-idf/blob/master/examples/wifi/power_save/main/power_save.c

  delay(300);
  //IPAddress ip = WiFi.localIP();
  //Serial.printf("WiFi connected %d.%d.%d.%d\n",ip[0],ip[1],ip[2],ip[3]);  
  Serial.printf("Setup 1/3 - address : %s",WiFi.localIP().toString().c_str());
  

  Blynk.begin(auth, ssid, pass, server, port); 

  Serial.println("Setup 2/3 - Blynk connected");
    
  
  //SampleBufferInitI2S();
  ESP_ERROR_CHECK( adc1_config_width(ADC_WIDTH_BIT_12) );
  ESP_ERROR_CHECK( adc_gpio_init(ADC_UNIT_1, ADC_CHANNEL_0) );
  ESP_ERROR_CHECK( adc_gpio_init(ADC_UNIT_1, ADC_CHANNEL_3) );

  ESP_ERROR_CHECK( adc1_config_channel_atten(ADC1_CHANNEL_0,ADC_ATTEN_11db) ); 
  ESP_ERROR_CHECK( adc1_config_channel_atten(ADC1_CHANNEL_3,ADC_ATTEN_11db) ); 

  pinMode(25, OUTPUT);
  pinMode(36, INPUT);
  pinMode(39, INPUT);


  // configure LED PWM functionalitites
  ledcSetup(ledChannel, freq, resolution);
  
  // attach the channel to the GPIO to be controlled
  ledcAttachPin(25, ledChannel);

   
}

void loop() {
  // put your main code here, to run repeatedly:
  uint16_t* byteBuffer = NULL;

  ledcWrite(ledChannel, val);
  dacWrite(26, 255-val);
  delay (100);

  
  Blynk.virtualWrite(V10,  val ); 
  Blynk.virtualWrite(V12,  val ); 

  printf("VAL : %d : ", val);
  i2s.read(ADC1_CHANNEL_0, &byteBuffer);
  //i2s.dump(ADC1_CHANNEL_0);

  delay (200);
  printf("VAL : %d : ", 255-val);
  i2s.read(ADC1_CHANNEL_3, &byteBuffer);
  //i2s.dump(ADC1_CHANNEL_3);

/*  
  FillBufferI2S(0);
  FillBufferI2S(1);
 */
 
  val += inc;
  if (val > 255) {
    inc = -8;
    val = 255;
  }
  if (val < 0 ) {
    inc = +8;
    val = 0;
  }

//  printf("Analog %d\n",val);
  
  delay(1000);
}